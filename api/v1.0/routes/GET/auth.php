<?php

/* Authentication GET - Retrieve Sessions */

$app->get('/session', function() use ($app)	{

	// Init Response
	$response = array('status' => 200, 'message' => "session request success", 'data' => array());

	try {
		
		// Set data response to session return
		$response['data'] = SessionHelper::getSession();

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);

});
