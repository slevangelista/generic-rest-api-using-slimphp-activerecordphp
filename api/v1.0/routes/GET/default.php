<?php

/* Default Route for GET */

// ALL ACTIVE
$app->get('/:route', function($route) use ($app) {

	// Init Response
	$response = array('status' => 200, 'message' => "Request for {$route} successful", 'data' => array());

	try {
		
		// Determine model for selected route
		$model = ucwords($route);
		
		// Get All Records
		$query = $model::find_all_by_active(1);

		$result = array();
		foreach($query as $value)	{
			$result[] = $value->to_array();
		}

		$response['data'] = $result;

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);

});

// ALL
$app->get('/:route/all', function($route) use ($app) {

	// Init Response
	$response = array('status' => 200, 'message' => "Request for {$route} successful", 'data' => array());

	try {
		
		// Determine model for selected route
		$model = ucwords($route);
		
		// Get All Records
		$query = $model::all();

		$result = array();
		foreach($query as $value)	{
			$result[] = $value->to_array();
		}

		$response['data'] = $result;

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);

});

// ALL WITH ORDER
$app->get('/:route/order/:field/:order', function($route, $orderField, $order) use ($app) {

	// Init Response
	$response = array('status' => 200, 'message' => "Request for {$route} successful", 'data' => array());

	try {
		
		// Determine model for selected route
		$model = ucwords($route);
		
		// Get All Records
		$query = $model::find('all', array(
        	'order' => "{$orderField} {$order}"
        ));

		$result = array();
		foreach($query as $value)	{
			$result[] = $value->to_array();
		}

		$response['data'] = $result;

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);

});

// COUNT ALL
$app->get('/:route/countAll', function ($route)	{

	// Init Response
	$response = array('status' => 200, 'message' => "Request for {$route} successful", 'data' => array());

	try {
		
		// Determine model for selected route
		$model = ucwords($route);
		
		// Get All Records
		$query = $model::all();

		$result = array();
		foreach($query as $value)	{
			$result[] = $value->to_array();
		}

		$response['data'] = count($result);

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);

});

// COUNT ACTIVE
$app->get('/:route/count', function($route)	{

	// Init Response
	$response = array('status' => 200, 'message' => "Request for {$route} successful", 'data' => array());

	try {
		
		// Determine model for selected route
		$model = ucwords($route);
		
		// Get All Records
		$query = $model::find_all_by_active(1);

		$result = array();
		foreach($query as $value)	{
			$result[] = $value->to_array();
		}

		$response['data'] = count($result);

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);

});

// FIELDS
$app->get('/:route/field', function($route) use ($app)	{

	// Init Response
	$response = array('status' => 200, 'message' => "Request for {$route} fields successful", 'data' => array());

	try {
		
		// Determine model for selected route
		$model = ucwords($route);
		
		// Get All Records
		$query = $model::table()->columns;

		$result = array();
		$blacklist = array('id', 'unique_id', 'created_at', 'updated_at');
		foreach($query as $key => $value)	{
			$result[$key] = $value->name;

			foreach ($blacklist as $val)	{
				if ($value->name == $val)
					unset($result[$key]);
	
			}
		}

		$response['data'] = $result;

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);

});

// FIELDS ALL
$app->get('/:route/fieldAll', function($route) use ($app)	{
	// Init Response
	$response = array('status' => 200, 'message' => "Request for {$route} fields successful", 'data' => array());

	try {
		
		// Determine model for selected route
		$model = ucwords($route);
		
		// Get All Records
		$query = $model::table()->columns;

		$result = array();
		foreach($query as $key => $value)	{
			$result[$key] = $value->name;
		}

		$response['data'] = $result;

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);
});

// PAGINATION
$app->get('/:route/paginate/:page/:items', function($route, $page, $items) use ($app)	{

	// Init Response
	$response = array('status' => 200, 'message' => "Request for all {$route} fields successful", 'data' => array());

	try {
		
		// Pagination Logic
		$index = 0;
		for($i = 1; $i < $page; $i++) {
            $index = $index + $items;
        }

        // Determine model for selected route
        $model = ucwords($route);

        $query = $model::find('all', array(
        	'limit' => $items,
        	'offset' => $index
        ));

        $result = array();
		foreach($query as $value)	{
			$result[] = $value->to_array();
		}

		$response['data'] = $result;

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);	

});

// PAGINATION W/ ORDER
$app->get('/:route/paginate/:page/:items/order/:field/:order', function($route, $page, $items, $orderField, $order) use ($app)	{

	// Init Response
	$response = array('status' => 200, 'message' => "Request for {$route} successful", 'data' => array());

	try {
		
		// Pagination Logic
		$index = 0;
		for($i = 1; $i < $page; $i++) {
            $index = $index + $items;
        }

        // Determine model for selected route
        $model = ucwords($route);

        $query = $model::find('all', array(
        	'order' => "{$orderField} {$order}",
        	'limit' => $items,
        	'offset' => $index
        ));

        $result = array();
		foreach($query as $value)	{
			$result[] = $value->to_array();
		}

		$response['data'] = $result;

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);	

});

// BY FIELD
$app->get('/:route/:field/:field_data', function($route, $field, $field_data) use ($app)	{
 
	// Init Response
	$response = array('status' => 200, 'message' => "Request for {$route} successful", 'data' => array());

	try {
		
		// Determine model for selected route
		$model = ucwords($route);
		$condition = 'find_all_by_' . $field;

		$query = $model::$condition($field_data);

		$result = array();
		foreach($query as $value)	{
			$result[] = $value->to_array();
		}

		$response['data'] = $result;

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);

});
