<?php

/* Authentication POST - Login and Logout */

$app->post('/login', function() use ($app)	{

	// Init Response
	$response = array('status' => 200, 'message' => "Successfully logged in", 'data' => array());

	try {
		
		if ($data_url = $app->request->getBody())	{
			parse_str($data_url, $data);

			// Verify required params
			Helper::verifyRequiredParams(array('username', 'password'), $data);

			// Get record based on username
			$user = Users::find_by_username_and_active($data['username'], 1);

			// Check if user exists
			if ($user)	{

				// Check if password is correct
				if (PasswordHash::compare($user->password, $data['password']))	{

					// Get application key
					$app_key = AppKeys::first();

					// Set response to the ff:
					$response['data'] = array(
						'app_key' => $app_key->key,
						'id' => $user->id,
						'unique_id' => $user->unique_id,
						'username' => $user->username
					);

					if (!isset($_SESSION)) {
	                    session_start();
	                }

					/* Set Session */
					foreach($response['data'] as $key => $value)	{
						$_SESSION[$key] = $value;
					}

				} else {
					throw new Exception ("Incorrect credentials given");
				}

			} else {
				throw new Exception ("No such user is registered");
			}

		} else {
			throw new Exception('No data parameters set');
		}

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);

});

$app->post('/logout', function() use ($app)	{

	// Init Response
	$response = array('status' => 200, 'message' => "Successfully logged out", 'data' => array());

	try {
		
		if ($data_url = $app->request->getBody())	{
			parse_str($data_url, $data);

			// Get App Key
			$app_key = AppKeys::first();

			if (isset($data['app_key']) && $app_key->key == $data['app_key'])	{

				// Unset data['app_key']
				unset($data['app_key']);

				SessionHelper::destroySession();

			} else {
				throw new Exception('You are not authorized to access this route');
			}

		} else {
			throw new Exception('No data parameters set');
		}

	} catch (Exception $e)	{
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);

});