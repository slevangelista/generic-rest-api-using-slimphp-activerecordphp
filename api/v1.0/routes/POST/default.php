<?php

/* Default Route for POST */

$app->post('/:route', function($route) use ($app) {

	// Init Response
	$response = array('status' => 200, 'message' => "Insert for {$route} successful", 'data' => array());

	try {
		
		if ($data_url = $app->request->getBody())	{
			parse_str($data_url, $data);

			// Get App Key
			$app_key = AppKeys::first();

			if (isset($data['app_key']) && $app_key->key == $data['app_key'])	{

				// Unset data['app_key']
				unset($data['app_key']);

				// Determine model for selected route
				$model = ucwords($route);

				// Begin Insert Process
				$query = new $model;
				foreach($data as $key => $value)	{
					$query->$key = $value;
				}

				// Add create time and update time
				$query->created_at = date('Y-m-d H:i:s');
				$query->updated_at = date('Y-m-d H:i:s');

				// Add unique id to table
				$query->unique_id = Helper::randomHash(35);

				$query->save();

				// If there are no errors, echo new inserted data from response
				$response['data'] = $query->to_array();

			} else {
				throw new Exception('You are not authorized to access this route');
			}

		} else {
			throw new Exception('No data parameters set');
		}

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);

});