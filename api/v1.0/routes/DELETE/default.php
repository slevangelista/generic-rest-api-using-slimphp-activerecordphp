<?php

/* Default Route for DELETE */

// SOFT DELETE (Active -> 0)
$app->delete('/:route/:field/:field_data', function($route, $field, $field_data) use ($app) {

	// Init Response
	$response = array('status' => 200, 'message' => "Soft delete for {$route} successful", 'data' => array());

	try {

		if ($data_url = $app->request->getBody())	{
			parse_str($data_url, $data);
		
			// Get App Key
			$app_key = AppKeys::first();

			if (isset($data['app_key']) && $app_key->key == $data['app_key'])	{

				// Unset data['app_key']
				unset($data['app_key']);

				// Determine model for selected route
				$model = ucwords($route);
				
				// Find Model Index
				$condition = 'find_all_by_' . $field;
				$query = $model::$condition($field_data);
				
				$result = array();
				foreach($query as $value)	{
				
					// Set active to 0
					$value->active = 0;

					// Update time
					$value->updated_at = date('Y-m-d H:i:s');

					$value->save();

					// Get result for response
					$result[] = $value->to_array();
				}

				// If there are no errors, echo new inserted data from response
				$response['data'] = $result;

			} else {
				throw new Exception('You are not authorized to access this route');
			}

		} else {
			throw new Exception('No data parameters set');
		}

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);

});

// HARD DELETE (Active -> 0)
$app->delete('/:route/hd/:field/:field_data', function($route, $field, $field_data) use ($app) {

	// Init Response
	$response = array('status' => 200, 'message' => "Hard delete for {$route} successful", 'data' => array());

	try {
		
		if ($data_url = $app->request->getBody())	{
			parse_str($data_url, $data);

			// Get App Key
			$app_key = AppKeys::first();

			if (isset($data['app_key']) && $app_key->key == $data['app_key'])	{

				// Unset data['app_key']
				unset($data['app_key']);

				// Determine model for selected route
				$model = ucwords($route);
				
				// Find Model Index
				$condition = 'find_all_by_' . $field;
				$query = $model::$condition($field_data);
				
				$result = array();
				foreach($query as $value)	{

					// Get result for response
					$result[] = $value->to_array();
				
					// Hard Delete
					$value->delete();
				}

				// If there are no errors, echo new inserted data from response
				$response['data'] = $result;

			} else {
				throw new Exception('You are not authorized to access this route');
			}

		} else {
			throw new Exception('No data parameters set');
		}

	} catch (Exception $e) {
		$response['status'] = 400;
		$response['message'] = $e->getMessage();
		$response['data'] = array();
	}

	Helper::JSONResponse($response);

});