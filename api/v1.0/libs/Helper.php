<?php

/* Basic Helper Class */

class Helper {

	public static function JSONResponse($response)	{
		$app = \Slim\Slim::getInstance();

		if (isset($response['status']))
			$app->status($response['status']);
		else
			throw new Exception("Status missing in JSONResponse");

	    $app->contentType('application/json');
	    echo json_encode($response);
	}

	public static function randomString($length = 22)	{
		return substr(sha1(mt_rand()), 0, $length);
	}

	public static function randomHash($length = 22)	{
		return PasswordHash::encode(substr(sha1(mt_rand()), 0, $length));
	}

	public static function verifyRequiredParams($required_fields, $request_params) {
	    $error = false;
	    $error_fields = "";
	    foreach ($required_fields as $field) {
	        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
	            $error = true;
	            $error_fields .= $field . ', ';
	        }
	    }

	    if ($error) {
	        // Required field(s) are missing or empty
	        // echo error json and stop the app
	        $response = array();
	        $app = \Slim\Slim::getInstance();

	        throw new Exception('Required field(s) ' . substr($error_fields, 0, -2) . ' is/are missing or empty');	   
	    }
	}

}