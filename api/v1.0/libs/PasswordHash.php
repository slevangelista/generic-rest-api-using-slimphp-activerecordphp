<?php

/* Php Password Hash encoder/comparator using crypt */
class PasswordHash {

	// blowfish
    private static $algo = '$2a';
    // cost parameter
    private static $cost = '$10';

    // mainly for internal use
    static function unique_salt() {
        return substr(sha1(mt_rand()), 0, 22);
    }

    // this will be used to generate a hash
    public static function encode($password) {
        return crypt($password, self::$algo .
            self::$cost . '$' . self::unique_salt());
    }

    // this will be used to compare a password against a hash
    public static function compare($hash, $password) {
        $full_salt = substr($hash, 0, 29);
        $new_hash = crypt($password, $full_salt);
        return ($hash == $new_hash);
    }

}