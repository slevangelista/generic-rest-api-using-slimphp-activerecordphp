<?php

class SessionHelper {

    function __construct()  {
        // Do nothing ...
    }

    public function getSession(){
        if (!isset($_SESSION)) {
            session_start();
        }
        $sess = array();
        if (isset($_SESSION['id']))  {
            $sess["app_key"] = $_SESSION['app_key'];
            $sess["id"] = $_SESSION['id'];
            $sess["unique_id"] = $_SESSION['unique_id'];
            $sess["username"] = $_SESSION['username'];
        }
        else   {
            $sess["app_key"] = NULL;
            $sess["id"] = NULL;
            $sess["unique_id"] = NULL;
            $sess["username"] = 'Guest';
        }
        
        return $sess;
    }

    public function destroySession(){
        if (!isset($_SESSION)) {
            session_start();
        }

        if(isset($_SESSION['id']))  {
            unset($_SESSION['app_key']);
            unset($_SESSION['id']);
            unset($_SESSION['unique_id']);
            unset($_SESSION['username']);

            $info='info';
            if(isset($_COOKIE[$info]))
            {
                setcookie ($info, '', time() - $cookie_time);
            }
            $msg="Logged Out Successfully...";
        }
        else{
            $msg = "Not logged in...";
        }
        return $msg;
    }
 
}
