<?php
 
/* Require Config */
require_once "./config.php";

/* Load Composer Vendor */
require_once "../vendor/autoload.php";

/* Load Libraries */
require_once "./libs/Helper.php";
require_once "./libs/PasswordHash.php";
require_once "./libs/SessionHelper.php";

/* Load DB Setup */
try {
	
    // Setup DB Connections
    $connections = array();
    foreach ($db as $key => $value) {
        $connections[$key] = 'mysql://' . $value['user'] . ':' . $value['pass'] . '@' . $value['host'] . '/' . $value['name'];
    }

	ActiveRecord\Config::initialize(function($cfg) use ($connections)	{
		$cfg->set_model_directory('models');
		$cfg->set_connections($connections);
	});

    // Set Base Class Settings for models
    abstract class ModelApp extends ActiveRecord\Model {
        static $connection = 'app';
    }

} catch (Exception $e) {
	echo "ActiveRecord Configuration Exception: " . $e->getMessage();    	
}

/* Load Routes */
$app = new \Slim\Slim();

// Default Route (Wildcard route for All)
require_once "routes/GET/default.php";
require_once "routes/POST/default.php";
require_once "routes/PUT/default.php";
require_once "routes/DELETE/default.php";

/* Run Route */
$app->run();